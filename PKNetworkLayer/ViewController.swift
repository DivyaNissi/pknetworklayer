//
//  ViewController.swift
//  PKNetworkLayer
//
//  Created by Divya Kothagattu on 31/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
        private let sessionProvider = URLSessionProvider()
        @IBOutlet var showMsgLbl : UILabel!
        override func viewDidLoad() {
            super.viewDidLoad()
            getPosts()
        }
    
        private func getPosts() {
            sessionProvider.request(type: [Post].self, service: PostService.all) { response in
                switch response {
                case let .success(posts):
                    DispatchQueue.main.async {
                    self.showMsgLbl?.text = "success"
                    }
                    print(posts)
                case let .failure(error):
                    DispatchQueue.main.async {
                    self.showMsgLbl?.text = "Failure"
                    }
                    print(error)
                }
            }
        }
    }


