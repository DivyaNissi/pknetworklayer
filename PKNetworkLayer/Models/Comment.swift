//
//  Comment.swift
//  PKNetworkLayer
//
//  Created by Divya Kothagattu on 31/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation

struct Comment: Codable {
    let id: Int
    let name: String
    let body: String
}
