//
//  ServiceProtocol.swift
//  PKNetworkLayer
//
//  Created by Divya Kothagattu on 31/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

import Foundation

typealias Headers = [String: String]

protocol ServiceProtocol {
    var baseURL: URL { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var task: Task { get }
    var headers: Headers? { get }
    var parametersEncoding: ParametersEncoding { get }
}
