//
//  NetworkError.swift
//  PKNetworkLayer
//
//  Created by Divya Kothagattu on 31/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

enum NetworkError {
    case unknown
    case noJSONData
}
