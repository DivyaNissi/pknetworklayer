//
//  NetworkResponse.swift
//  PKNetworkLayer
//
//  Created by Divya Kothagattu on 31/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

enum NetworkResponse<T> {
    case success(T)
    case failure(NetworkError)
}
