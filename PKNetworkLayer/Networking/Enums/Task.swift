//
//  Task.swift
//  PKNetworkLayer
//
//  Created by Divya Kothagattu on 31/03/20.
//  Copyright © 2020 Divya Kothagattu. All rights reserved.
//

typealias Parameters = [String: Any]

enum Task {
    case requestPlain
    case requestParameters(Parameters)
}
